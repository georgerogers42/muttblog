extern crate serde_codegen;

use std::fs;
use std::env;
use std::path::Path;

fn main() {
    let out_dir = env::var_os("OUT_DIR").unwrap();

    let src = Path::new("src/app/articles/mod.rs.in");
    let dst = Path::new(&out_dir).join("app/articles/mod.rs");

    fs::create_dir_all(Path::new(&out_dir).join("app/articles")).unwrap();
    serde_codegen::expand(&src, &dst).unwrap();
}
