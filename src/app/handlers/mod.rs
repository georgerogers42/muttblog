use std::collections::HashMap;
use rustc_serialize::json::ToJson;
use hbs;
use iron::prelude::*;
use iron::status;
use router::Router;
use app::articles::*;

pub fn home(arts: &[Article], _req: &mut Request) -> IronResult<Response> {
    let mut data = HashMap::new();
    data.insert("articles".to_string(), arts.to_json());
    Ok(Response::with((status::Ok, hbs::Template::new("articles", data.to_json()))))
}

pub fn article(arts: &HashMap<String, Article>, req: &mut Request) -> IronResult<Response> {
    let slug = req.extensions.get::<Router>().unwrap().find("slug").unwrap();
    let mut data = HashMap::new();
    data.insert("articles".to_string(), vec![arts.get(slug).unwrap().to_json()].to_json());
    Ok(Response::with((status::Ok, hbs::Template::new("articles", data.to_json()))))
}
