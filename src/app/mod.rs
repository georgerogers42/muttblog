extern crate iron;
extern crate router;
extern crate mount;
extern crate staticfile;
extern crate handlebars_iron as hbs;
extern crate rustc_serialize;
extern crate glob;
extern crate chrono;

use iron::prelude::*;
use std::sync::Arc;
use mount::Mount;
use staticfile::Static;

mod handlers;
mod articles;

pub type App = Mount;

pub fn application() -> App {
    let mut r = router::Router::new();
    let mut a = articles::load("articles/*.html").unwrap();
    a.sort_by(|x, y| y.metadata.date.cmp(&x.metadata.date));
    let art_map = Arc::new(articles::mapping(&a));
    let arts = Arc::new(a);
    {
        let arts = arts.clone();
        r.get("/", move |r: &mut Request| handlers::home(&arts, r));
    }
    {
        let arts = arts.clone();
        r.get("/articles/", move |r: &mut Request| handlers::home(&arts, r));
    }
    {
        let art_map = art_map.clone();
        r.get("/articles/:slug", move |r: &mut Request| handlers::article(&art_map, r));
    }
    let mut routes = Chain::new(r);
    let mut hbse = hbs::HandlebarsEngine::new();
    hbse.add(Box::new(hbs::DirectorySource::new("./templates/", ".hbs")));
    hbse.reload().unwrap();
    routes.link_after(hbse);
    let mut app = Mount::new();
    app.mount("/", routes);
    app.mount("/css/", Static::new("public/css/"));
    app.mount("/js/", Static::new("public/js/"));
    app.mount("/fonts/", Static::new("public/fonts/"));
    app.mount("/pictures/", Static::new("public/pictures/"));
    app
}

pub fn start(on: &str) {
    Iron::new(application()).http(on).unwrap();
}
